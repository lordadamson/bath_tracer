#include "v3.h"
#include "util.h"

v3
v3_not(v3 v)
{
    v3 not_v = {-v.x, -v.y, -v.y};
    return not_v;
}

v3
v3_add(v3 first, v3 second)
{
    v3 result = {0};
    result.x = first.x + second.x;
    result.y = first.y + second.y;
    result.z = first.z + second.z;
    return result;
}

v3
v3_sub(v3 first, v3 second)
{
    v3 result = {0};
    result.x = first.x - second.x;
    result.y = first.y - second.y;
    result.z = first.z - second.z;
    return result;
}

v3
v3_mul(v3 first, v3 second)
{
    v3 result = {};
    result.x = first.x * second.x;
    result.y = first.y * second.y;
    result.z = first.z * second.z;
    return result;
}

v3
v3_scale(v3 v, float t)
{
    v.x *= t;
    v.y *= t;
    v.z *= t;
    return v;
}

float
v3_dot(v3 u, v3 v)
{
    return u.x * v.x
         + u.y * v.y
         + u.z * v.z;
}

v3
v3_cross(v3 u, v3 v)
{
    v3 result = {};

    result.x = u.y * v.z - u.z * v.y;
    result.y = u.z * v.x - u.x * v.z;
    result.z = u.x * v.y - u.y * v.x;

    return result;
}

float
v3_length_squared(v3 self)
{
    return self.x*self.x + self.y*self.y + self.z*self.z;
}

float
v3_length(v3 self)
{
    return sqrt(v3_length_squared(self));
}

v3
v3_unit_vector(v3 v)
{
    v = v3_scale(v, 1.0f/v3_length(v));
    return v;
}

v3
v3_rnd()
{
    return (v3){rnd(), rnd(), rnd()};
}

v3
v3_rnd_rng(float min, float max)
{
    return (v3){rnd_rng(min, max), rnd_rng(min, max), rnd_rng(min, max)};
}

v3
v3_rnd_in_unit_sphere()
{
    while(1)
    {
        v3 p = v3_rnd_rng(-1.0f, 1.0f);
        if (v3_length_squared(p) >= 1) continue;
        return p;
    }
}

v3
rnd_unit_vector()
{
    return v3_unit_vector(v3_rnd_in_unit_sphere());
}

v3
random_in_hemisphere(v3 normal)
{
    v3 in_unit_sphere = v3_rnd_in_unit_sphere();

    if (v3_dot(in_unit_sphere, normal) > 0.0f) // In the same hemisphere as the normal
    {
        return in_unit_sphere;
    }
    else
    {
        return v3_not(in_unit_sphere);
    }
}

bool
v3_near_zero(v3 v)
{
    // Return true if the vector is close to zero in all dimensions.
    const float s = 1e-8f;
    return (fabs(v.x) < s) && (fabs(v.y) < s) && (fabs(v.z) < s);
}

void
v3_print(v3 self, FILE* out)
{
    fprintf(out, "%f %f %f", self.x, self.y, self.y);
}