#pragma once

#include <v3.h>
#include <util.h>
#include <image.h>

typedef v3 color;

void
color_print(color self, image output, size_t offset, int samples_per_pixel);
