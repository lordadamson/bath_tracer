import os
from ctypes import *

class v3(Structure):
	_fields_ = [
		("x", c_float),
		("y", c_float),
		("z", c_float)
	]

class camera_options(Structure):
	_fields_ = [
		("lookfrom", v3),
		("lookat", v3),
		("vup", v3),
		("vfov", c_float),
		("image_width", c_int),
		("image_height", c_int)
	]

class camera(Structure):
	_fields_ = [
		("origin", v3),
		("lower_left_corner", v3),
		("horizontal", v3),
		("vertical", v3)
	]

class bath_tracer_opts(Structure):
	_fields_ = [
		("aspect_ratio", c_float),
		("image_width", c_int),
		("image_height", c_int),
		("samples_per_pixel", c_int),
		("max_depth", c_int),
		("cam", camera),
	]

class material_rizz(Union):
	_fields_ = [
		("fuzz", c_float),
		("ir", c_float)
	]

MATERIAL_TYPE_NONE = 0
MATERIAL_TYPE_LAMBERTIAN = 1
MATERIAL_TYPE_METAL = 2
MATERIAL_TYPE_DIELECTRIC = 3

class material(Structure):
	_fields_ = [
		("type", c_int),
		("albedo", v3),
		("rizz", material_rizz)
	]

class sphere(Structure):
	_fields_ = [
		("center", v3),
		("radius", c_float),
		("mat", material)
	]

class sphere_list(Structure):
	_fields_ = [
		("spheres", sphere * 150),
		("count", c_uint)
	]

class c_image(Structure):
	_fields_ = [
		("pixels", POINTER(c_ubyte)),
		("w", c_uint64),
		("h", c_uint64),
		("c", c_uint64)
	]

class image:
	def __init__(self, pixels = None, w = 0, h = 0, c = 0):
		self.pixels = pixels
		self.w = w
		self.h = h
		self.c = c

def bath_trace():
	bath_tracer = cdll.LoadLibrary(os.path.dirname(os.path.realpath(__file__)) + "/libbath_tracer.dylib")

	aspect_ratio = 16.0 / 9.0
	image_width = 800
	image_height = int(image_width / aspect_ratio)

	cam_opts = camera_options(
		image_width = int(image_width),
		image_height = int(image_height),
		vfov = 90.0,
		lookfrom = v3(-2.0, 2.0, 1.0),
		lookat = v3(0.0, 0.0, -1.0),
		vup = v3(0.0, 1.0, 0.0)
	)

	bath_tracer.camera_make.restype = camera
	cam = bath_tracer.camera_make(cam_opts);

	bt = bath_tracer_opts (
		aspect_ratio = aspect_ratio,
		image_width = int(image_width),
		image_height = int(image_height),
		samples_per_pixel = 50,
		max_depth = 50,
		cam = cam
	)

	s1 = sphere(
		radius = 100.0,
		center = v3(0.0, -100.5, -1.0),
		mat = material(
			type = MATERIAL_TYPE_LAMBERTIAN,
			albedo = v3(0.8, 0.8, 0.0)
		)
	)

	s2 = sphere(
		radius = 0.5,
		center = v3(0.0, 0.0, -1.0),
		mat = material(
			type = MATERIAL_TYPE_LAMBERTIAN,
			albedo = v3(0.1, 0.2, 0.5)
		)
	)

	s3 = sphere(
		radius = 0.5,
		center = v3(-1.0, 0.0, -1.0),
		mat = material(
			type = MATERIAL_TYPE_DIELECTRIC,
			rizz = material_rizz(ir = 1.5)
		)
	)

	s4 = sphere(
		radius = -0.4,
		center = v3(-1.0, 0.0, -1.0),
		mat = material(
			type = MATERIAL_TYPE_DIELECTRIC,
			rizz = material_rizz(ir = 1.5)
		)
	)

	s5 = sphere(
		radius = 0.5,
		center = v3(1.0, 0.0, -1.0),
		mat = material(
			type = MATERIAL_TYPE_METAL,
			albedo = v3(0.8, 0.6, 0.2),
			rizz = material_rizz(ir = 0.0)
		)
	)

	l = sphere_list();

	bath_tracer.sphere_list_push(pointer(l), s1);
	bath_tracer.sphere_list_push(pointer(l), s2);
	bath_tracer.sphere_list_push(pointer(l), s3);
	bath_tracer.sphere_list_push(pointer(l), s4);
	bath_tracer.sphere_list_push(pointer(l), s5);

	bath_tracer.bath_trace.restype = c_image
	img = bath_tracer.bath_trace(bt, l)	

	float_pixels = []
	for i in range(3, img.w*img.h*img.c, 4):
		float_pixels.append(
			[
				float(img.pixels[i-3])/255.0,
				float(img.pixels[i-2])/255.0,
				float(img.pixels[i-1])/255.0,
				float(img.pixels[i])/255.0,
			]
		)

	bath_tracer.image_free(img)

	img = image(float_pixels, img.w, img.h, img.c)

	return img

if __name__ == '__main__':
	bath_trace()
