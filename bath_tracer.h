#include <image.h>
#include <camera.h>
#include <traceables.h>

struct bath_tracer_opts
{
	float aspect_ratio;
    int image_width;
    int image_height;
    int samples_per_pixel;
    int max_depth;
    camera cam;
};

typedef struct bath_tracer_opts bath_tracer_opts;

image
bath_trace(bath_tracer_opts bt, sphere_list l);