#include <image.h>

#include <string.h>
#include <stdlib.h>

image
image_new(size_t width, size_t height, size_t channels)
{
    image img = {
        .w = width,
        .h = height,
        .c = channels
    };

    img.pixels = malloc(img.w * img.h * img.c);
    memset(img.pixels, 0, img.w * img.h * img.c);
    return img;
}

void
image_free(image img)
{
    free(img.pixels);
}

size_t
image_at(image img, size_t x, size_t y)
{
    return img.c * (img.w * y + x);
}