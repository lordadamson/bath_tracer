#pragma once

#include <v3.h>
#include <color.h>

struct material
{
    enum type_ {
        none,
        lambertian,
        metal,
        dielectric
    } type;

    color albedo;
    union
    {
        float fuzz;
        float ir;
    } u;
};

typedef struct material material;

struct sphere
{
    p3 center;
    float radius;
    material mat;
};

typedef struct sphere sphere;

struct sphere_list
{
    sphere spheres[128];
    size_t count;
};

typedef struct sphere_list sphere_list;

void
sphere_list_push(sphere_list* l, sphere s);