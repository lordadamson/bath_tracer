#pragma once

#include <stddef.h>
#include <stdint.h>

struct image
{
    uint8_t* pixels;
    uint64_t w, h, c;
};

typedef struct image image;

image
image_new(size_t width, size_t height, size_t channels);

void
image_free(image img);

size_t
image_at(image img, size_t x, size_t y);