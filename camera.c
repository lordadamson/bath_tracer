#include "camera.h"

#include "util.h"

camera
camera_make(camera_options opts)
{
	const float aspect_ratio = (float)opts.image_width / (float)opts.image_height;

    float theta = degrees_to_radians(opts.vfov);
    float h = tan(theta/2);
    float viewport_height = 2.0 * h;
    float viewport_width = aspect_ratio * viewport_height;

    v3 w = v3_unit_vector(v3_sub(opts.lookfrom, opts.lookat));
    v3 u = v3_unit_vector(v3_cross(opts.vup, w));
    v3 v = v3_cross(w, u);

    camera c = {0};

    c.origin = opts.lookfrom;
    c.horizontal = v3_scale(u, viewport_width);
    c.vertical = v3_scale(v, viewport_height);
    c.lower_left_corner = c.origin;
    c.lower_left_corner = v3_sub(c.lower_left_corner, v3_scale(c.horizontal, 0.5));
    c.lower_left_corner = v3_sub(c.lower_left_corner, v3_scale(c.vertical, 0.5));
    c.lower_left_corner = v3_sub(c.lower_left_corner, w);

    return c;
}

ray
camera_get_ray(camera c, float s, float t)
{
    v3 dir = c.lower_left_corner;
    v3 dirh = v3_scale(c.horizontal, s);
    v3 dirv = v3_scale(c.vertical,   t);
    dir = v3_add(dir, dirh);
    dir = v3_add(dir, dirv);
    dir = v3_sub(dir, c.origin);

    ray r = {
        c.origin,
        dir
    };

    return r;
}
