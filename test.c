#include <bath_tracer.h>

int main()
{
    const float aspect_ratio = 16.0f / 9.0f;
    const int image_width = 800;
    const int image_height = (int)(image_width / aspect_ratio);

    camera_options opts = {
        .image_width = image_width,
        .image_height = image_height,
        .vfov = 20.0f,
        .lookfrom = (p3){-2.0f, 2.0f, 1.0f},
        .lookat = (p3){0.0f, 0.0f, -1.0f},
        .vup = (p3){0.0f, 1.0f, 0.0f}
    };

    camera c = camera_make(opts);

    bath_tracer_opts bt = {
        .cam = c,
        .aspect_ratio = aspect_ratio,
        .image_width = image_width,
        .image_height = image_height,
        .samples_per_pixel = 50,
        .max_depth = 20,
    };
    
    sphere s1 = {
        .radius = 100.0f,
        .center = { 0.0f, -100.5f, -1.0f },
        .mat = {
            .type = lambertian,
            .albedo = (color){0.8f, 0.8f, 0.0f}
        }
    };

    sphere s2 = {
        .radius = 0.5f,
        .center = { 0.0f, 0.0f, -1.0f },
        .mat = {
            .type = lambertian,
            .albedo = (color){0.1f, 0.2f, 0.5f},
        }
    };

    sphere s3 = {
        .radius = 0.5f,
        .center = { -1.0f, 0.0f, -1.0f },
        .mat = {
            .type = dielectric,
            .u.fuzz = 1.5f
        }
    };

    sphere s4 = {
        .radius = -0.4f,
        .center = { -1.0f, 0.0f, -1.0f },
        .mat = {
            .type = dielectric,
            .u.fuzz = 1.5f
        }
    };

    sphere s5 = {
        .radius = 0.5f,
        .center = { 1.0f, 0.0f, -1.0f },
        .mat = {
            .type = metal,
            .albedo = (color){0.8f, 0.6f, 0.2f},
            .u.ir = 0.0f
        }
    };

    sphere_list l = {0};

    sphere_list_push(&l, s1);
    sphere_list_push(&l, s2);
    sphere_list_push(&l, s3);
    sphere_list_push(&l, s4);
    sphere_list_push(&l, s5);

    bath_trace(bt, l);
}
