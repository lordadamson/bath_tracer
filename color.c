#include <color.h>

#include <math.h>

void
color_print(color self, image output, size_t offset, int samples_per_pixel)
{
    float r = self.x;
    float g = self.y;
    float b = self.z;

    // Divide the color by the number of samples and gamma-correct for gamma=2.0.
    float scale = 1.0 / samples_per_pixel;
    r = sqrt(scale * r);
    g = sqrt(scale * g);
    b = sqrt(scale * b);

    output.pixels[offset]   = (uint8_t)(256 * clamp(r, 0.0, 0.999));
    output.pixels[offset+1] = (uint8_t)(256 * clamp(g, 0.0, 0.999));
    output.pixels[offset+2] = (uint8_t)(256 * clamp(b, 0.0, 0.999));
}
