#!/usr/bin/env bash

set -ex

cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug
make -j 4
cd ../blender_addon
time python3 bath_tracer.py > img.ppm
open /Users/adam/Downloads/img.jpg