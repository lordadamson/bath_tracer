#pragma once

#include "v3.h"
#include "ray.h"

struct camera
{
	p3 origin;
	p3 lower_left_corner;
	v3 horizontal;
	v3 vertical;
};

typedef struct camera camera;

struct camera_options
{
	p3 lookfrom;
    p3 lookat;
    v3 vup;
    float vfov;
    int image_width;
    int image_height;
};

typedef struct camera_options camera_options;

camera
camera_make(camera_options options);

ray
camera_get_ray(camera c, float s, float t);