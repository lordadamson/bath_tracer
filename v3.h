#pragma once

#include <math.h>
#include <stdio.h>
#include <stdbool.h>

struct v3
{
    float x, y, z;
};

typedef struct v3 v3;
typedef v3 p3;

v3
v3_not(v3 v);

v3
v3_add(v3 first, v3 second);

v3
v3_sub(v3 first, v3 second);

v3
v3_mul(v3 first, v3 second);

v3
v3_scale(v3 v, float t);

float
v3_dot(v3 u, v3 v);

v3
v3_cross(v3 u, v3 v);

float
v3_length_squared(v3 self);

float
v3_length(v3 self);

v3
v3_unit_vector(v3 v);

v3
v3_rnd();

v3
v3_rnd_rng(float min, float max);

v3
v3_rnd_in_unit_sphere();

v3
rnd_unit_vector();

v3
random_in_hemisphere(v3 normal);

bool
v3_near_zero(v3 v);

void
v3_print(v3 self, FILE* out);
