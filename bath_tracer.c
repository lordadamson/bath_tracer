#include <math.h>
#include <stdio.h>
#include <float.h>
#include <stdbool.h>

#include "v3.h"
#include "ray.h"
#include "util.h"
#include "color.h"
#include "camera.h"
#include "bath_tracer.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#define DIFFUSE
//#define LAMBERTIAN
//#define HEMISPHERICAL

struct hit_record
{
    p3 p;
    v3 normal;
    float t;
    material mat;
    bool front_face;
    bool hit;
};

typedef struct hit_record hit_record;

static inline void
set_face_normal(hit_record* rec, ray r, v3 outward_normal)
{
    rec->front_face = v3_dot(r.dir, outward_normal) < 0;
    rec->normal = rec->front_face ? outward_normal : v3_not(outward_normal);
}

static inline hit_record
sphere_hit(sphere s, ray r, float t_min, float t_max)
{
    v3 oc = v3_sub(r.orig, s.center);

    float a = v3_length_squared(r.dir);
    float half_b = v3_dot(oc, r.dir);
    float c = v3_length_squared(oc) - (s.radius*s.radius);
    float discriminant = half_b*half_b - a*c;

    if (discriminant < 0)
    {
            return (hit_record){0};
    }

    float sqrtd = sqrt(discriminant);

    // Find the nearest root that lies in the acceptable range.
    float root = (-half_b - sqrtd) / a;

    if (root < t_min || t_max < root)
    {
        root = (-half_b + sqrtd) / a;

        if (root < t_min || t_max < root)
        {
            return (hit_record){0};
        }
    }

    hit_record rec = {0};
    rec.hit = true;
    rec.t = root;
    rec.p = ray_at(r, rec.t);
    v3 outward_normal = v3_sub(rec.p, s.center);
    outward_normal = v3_scale(outward_normal, 1.0f/s.radius);
    set_face_normal(&rec, r, outward_normal);
    rec.mat = s.mat;

    return rec;
}

hit_record
hits(const sphere_list* list, ray r, float t_min, float t_max)
{
    hit_record rec = {0};
    float closest_so_far = t_max;

    for(size_t i = 0; i < list->count; i++)
    {
        hit_record tmp = sphere_hit(list->spheres[i], r, t_min, closest_so_far);
        
        if (!tmp.hit)
        {
            continue;
        }
        
        closest_so_far = tmp.t;
        rec = tmp;
    }

    return rec;
}

struct scatter_t
{
    color attenuation;
    ray scattered;
    bool should_trace;
};

typedef struct scatter_t scatter_t;

v3
reflect(v3 v, v3 n)
{
    return v3_sub(v, v3_scale(n, 2 * v3_dot(v, n)));
}

float
reflectance(float cosine, float ref_idx)
{
    // Use Schlick's approximation for reflectance.
    float r0 = (1.0f-ref_idx) / (1.0f+ref_idx);
    r0 = r0*r0;
    return r0 + (1.0f-r0)*pow((1.0f - cosine), 5.0f);
}

v3
refract(v3 uv, v3 n, float etai_over_etat)
{
    float cos_theta = fmin(v3_dot(v3_not(uv), n), 1.0);
    v3 r_out_perp = v3_scale(n, cos_theta);
    r_out_perp = v3_add(r_out_perp, uv);
    r_out_perp = v3_scale(r_out_perp, etai_over_etat);

    v3 r_out_parallel = v3_scale(n, -sqrt(fabs(1.0 - v3_length_squared(r_out_perp))));
    return v3_add(r_out_perp, r_out_parallel);
}

scatter_t
scatter(ray r_in, hit_record rec)
{
    scatter_t s = {0};

    v3 scatter_direction = {0};

    switch (rec.mat.type)
    {
    case lambertian:
        #ifdef DIFFUSE
        scatter_direction = v3_add(rec.normal, v3_rnd_in_unit_sphere());
        #elif defined(LAMBERTIAN)
        scatter_direction = v3_add(rec.normal, rnd_unit_vector());
        #elif defined(HEMISPHERICAL)
        scatter_direction = v3_add(rec.normal, random_in_hemisphere(rec.normal));
        #else
        #error "one of DIFFUSE, LAMBERTIAN, or HEMISPHERICAL must be defined"
        #endif

        if (v3_near_zero(scatter_direction))
        {
            scatter_direction = rec.normal;
        }

        s.scattered = (ray){rec.p, scatter_direction};
        s.attenuation = rec.mat.albedo;
        s.should_trace = true;
        return s;
    case metal:
        scatter_direction = reflect(v3_unit_vector(r_in.dir), rec.normal);

        v3 target = v3_scale(v3_rnd_in_unit_sphere(), rec.mat.u.fuzz);
        s.scattered = (ray){rec.p, v3_add(scatter_direction, target)};
        s.attenuation = rec.mat.albedo;
        s.should_trace = v3_dot(s.scattered.dir, rec.normal) > 0;
        return s;
    case dielectric:
        s.attenuation = (color){1.0f, 1.0f, 1.0f};
        float refraction_ratio = rec.front_face ? (1.0f/rec.mat.u.ir) : rec.mat.u.ir;

        v3 unit_direction = v3_unit_vector(r_in.dir);

        float cos_theta = fmin(v3_dot(v3_not(unit_direction), rec.normal), 1.0f);
        float sin_theta = sqrt(1.0f - cos_theta*cos_theta);

        bool cannot_refract = refraction_ratio * sin_theta > 1.0f;
        v3 direction;

        if (cannot_refract || reflectance(cos_theta, refraction_ratio) > rnd())
            direction = reflect(unit_direction, rec.normal);
        else
            direction = refract(unit_direction, rec.normal, refraction_ratio);

        s.scattered = (ray){rec.p, direction};
        s.should_trace = true;
        return s;
    default:
        fprintf(stderr, "NANI!!!");
        abort();
    }    
}

static inline color
path_trace(struct ray r, const sphere_list* l, int depth)
{
    if (depth <= 0)
    {
        return (color){0};
    }

    hit_record rec = hits(l, r, 0.0001f, FLT_MAX);

    if(rec.hit)
    {
        scatter_t s = scatter(r, rec);

        if (s.should_trace)
        {
            return v3_mul(s.attenuation, path_trace(s.scattered, l, depth-1));
        }

        return (color){0,0,0};
    }

    v3 unit_direction = v3_unit_vector(r.dir);
    float t = 0.5f * (unit_direction.y + 1.0f);

    return v3_add(
        v3_scale((color){1.0f, 1.0f, 1.0f}, (1.0f-t)),
        v3_scale((color){0.5f, 0.7f, 1.0f}, t)
    );
}

image
bath_trace(bath_tracer_opts bt, sphere_list l)
{
    image output = image_new(bt.image_width, bt.image_height, 4);

    for (int j = bt.image_height-1; j >= 0; --j)
    {
        fprintf(stderr, "\rScanlines remaining: %d ", j);

        for (int i = 0; i < bt.image_width; ++i)
        {
            color pixel_color = {0};

            for(size_t k = 0; k < bt.samples_per_pixel; k++)
            {
                float u = (float)(i + rnd()) / (bt.image_width-1);
                float v = (float)(j + rnd()) / (bt.image_height-1);

                ray r = camera_get_ray(bt.cam, u, v);

                // TODO: make it so that the all the samples know the kind of material
                // to avoid the check everytime
                pixel_color = v3_add(pixel_color, path_trace(r, &l, bt.max_depth));
            }
            

            color_print(pixel_color, output, image_at(output, i, j), bt.samples_per_pixel);
        }
    }

    stbi_write_jpg(
        "/Users/adam/Downloads/img.jpg",
        output.w,
        output.h,
        output.c,
        output.pixels,
        100
    );

    fprintf(stderr, "\nDone.\n");
    return output;
}
