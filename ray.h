#pragma once

#include "v3.h"

struct ray
{
	p3 orig;
    v3 dir;
};

typedef struct ray ray;

static inline p3
ray_at(ray self, float t)
{
	// P(t) = A + tb
	return v3_add(self.orig, v3_scale(self.dir, t));
}